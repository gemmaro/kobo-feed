# 東京詰将棋工房のカレンダー

Guixで`guix shell`とすれば開発環境が準備されます。
`ruby main.rb`で`public/kobo.ics`にファイルがダウンロードされます。
`npm run deploy`とすると<https://kobo.surge.sh>にデプロイされます。
あとは<https://kobo.surge.sh/kobo.ics>からカレンダーを照会するだけです。

照会の方法はカレンダーのアプリによってまちまちです。
iPhoneの標準のカレンダーアプリでは以下です。

1. 下のメニューの「カレンダー」を選択。
1. 左下のメニューの「カレンダーを追加」を選択。
1. 選択肢「照会カレンダーを追加」を選択。
1. 照会URLに<https://kobo.surge.sh/kobo.ics>を入力。
1. 「照会」を押下。
1. タイトルを入力。
   例：「東京詰将棋工房」
1. アカウント、カラーを選択。
   その他の設定はお好みで。
1. 右上の「追加」を押下。

## 使用許諾

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
