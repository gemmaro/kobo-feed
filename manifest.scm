(use-modules (guix packages)
             (gnu packages ruby)
             (guix utils)
             ((guix licenses) #:prefix license:)
             (guix download)
             (guix build-system ruby)
             (guix git-download)
             (gnu packages rails))

(define-public ruby-nokogiri'
  (package
   (inherit ruby-nokogiri)
   (arguments
    (cons* #:ruby ruby-3.2
           (package-arguments ruby-nokogiri)))))

(define-public ruby-ice-cube
  (package
    (name "ruby-ice-cube")
    (version "0.16.4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ice-cube-ruby/ice_cube")
                    (commit "36f6d2414d3d06ee3f5e0b9484b03a2a754d4945")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0vfl54ad7abjxd8nng1vr2f6862b8lxbxq4sqqdyiq7ag8qx81iq"))))
    (build-system ruby-build-system)
    (arguments
     (list #:test-target "spec"
           #:tests? #f ;TODO
           ))
    (native-inputs (list ruby-rspec ruby-activesupport ruby-tzinfo ruby-tzinfo-data))
    (synopsis
     "Recurring date library for Ruby")
    (description
     "@code{ice_cube} is a recurring date library for Ruby.  It allows for
quick, programatic expansion of recurring date rules.")
    (home-page "https://github.com/ice-cube-ruby/ice_cube")
    (license license:expat)))

(define-public ruby-icalendar
  (package
    (name "ruby-icalendar")
    (version "2.8.0")
    (source (origin
              (method url-fetch)
              (uri (rubygems-uri "icalendar" version))
              (sha256
               (base32
                "11zfs0l8y2a6gpf0krm91d0ap2mnf04qky89dyzxwaspqxqgj174"))))
    (build-system ruby-build-system)
    (arguments
     (list #:test-target "spec"))
    (native-inputs (list ruby-rspec ruby-simplecov ruby-timecop ruby-tzinfo ruby-tzinfo-data))
    (propagated-inputs (list ruby-ice-cube))
    (synopsis
     "Implements the iCalendar specification (RFC-5545) in Ruby")
    (description
     "This package implements the iCalendar specification (RFC-5545) in
Ruby.  This allows for the generation and parsing of @code{.ics}
files, which are used by a variety of calendaring applications.")
    (home-page "https://github.com/icalendar/icalendar")
    (license license:ruby)))

(packages->manifest (list ruby-nokogiri' ruby-3.2 ruby-icalendar ruby-rubocop))
