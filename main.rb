require 'uri'
require 'net/http'
require 'nokogiri'
require 'pathname'
require 'logger'
require 'time'
require 'icalendar'

SCHEDULE_PATH = Pathname('schedule.html')
OUTPUT_ICAL = Pathname('public/kobo.ics')

LOG = Logger.new($stderr)

unless SCHEDULE_PATH.file?
  schedule = URI('https://tsume-kobo.org/ttmsch.html')
  source = Net::HTTP.get(schedule)
  SCHEDULE_PATH.write(source)
end

doc = Nokogiri::HTML(SCHEDULE_PATH)
entries = doc.xpath('/html/body/ul/li[1]/ul/li')

cal = Icalendar::Calendar.new

def full_width_digits_to_half_width_digits(str)
  str.tr('１２３４５６７８９０', '1234567890')
end

entries.each do |entry|
  title, _, _, location, room, _, _, apply, = entry.children
  title = title.content
  matches = title.match(/第(?<times>[^回]+)回　(?<month>[^月]{1,2})月(?<day>[^日]{1,2})日（(?<day_of_week>[^）])）(?<start>(?<start_hour>..)：(?<start_min>..))～(?<fin>(?<fin_hour>..)：(?<fin_min>..))/)
  times = full_width_digits_to_half_width_digits(matches[:times])
  month = full_width_digits_to_half_width_digits(matches[:month])
  day = full_width_digits_to_half_width_digits(matches[:day])
  start_hour = full_width_digits_to_half_width_digits(matches[:start_hour])
  start_min = full_width_digits_to_half_width_digits(matches[:start_min])
  fin_hour = full_width_digits_to_half_width_digits(matches[:fin_hour])
  fin_min = full_width_digits_to_half_width_digits(matches[:fin_min])
  start = Time.new(Time.now.year, month, day, start_hour, start_min)
  fin = Time.new(Time.now.year, month, day, fin_hour, fin_min)

  location = location.to_xhtml
  room = full_width_digits_to_half_width_digits(room.content.strip.sub(/^　+/, ''))
  apply = apply.to_xhtml if apply
  description = "場所：#{location} #{room}"
  description += "、応募：#{apply}" if apply

  cal.event do |e|
    e.dtstart = start
    e.dtend = fin
    e.summary = "東京詰将棋工房 第#{times}回"
    e.x_alt_desc = Icalendar::Values::Text.new(description, { 'FMTTYPE' => 'text/html' })
  end
end

ical = cal.to_ical
Dir.mkdir(OUTPUT_ICAL.dirname) unless File.directory?(OUTPUT_ICAL.dirname)
File.write(OUTPUT_ICAL, ical)
